# Summary
- Quick test using RabbitMQ message queues with SSE in Go and react frontend
- Uses direct and topic exchanges
- Not worried about missing SSE on client, so messages are transient, non-durable
- Also pub/subs to a persistent, durable message setup
- Will autoconnect if disconnected from RabbitMQ service
- Reuses connection for both Publishing functions
  - But doesn't reuse for Subscriber, as recommended
- Uses a lot from:
  - [`rabbitmq/amqp091-go` example of automatic reconnection](https://raw.githubusercontent.com/rabbitmq/amqp091-go/feb4ba5dd2d04ea8bdb625460478eff9c52eaa03/example_client_test.go)
  - [rabbitmq.com tutorials](https://www.rabbitmq.com/tutorials/tutorial-one-go.html)

# Quick notes on RabbitMQ
- Connection: Connect to rabbitmq service
  - Defines things like max no. of allowed channels
- Channel: like connection pools (Connections which share a tpc connection)
  - Open a Connection and use that to create Channel(s)
  - Ment to be long lived
  - Should be closed by the client once no longer needed to reduce resource usage(failing to do so results in build up on redis service - channel leak)
  - [ref](https://www.rabbitmq.com/channels.html)
- Queue: buffer that stores messages
  - Queue Binding: relationship between an Exchange and Queue
- Producer: application that sends messages to the Exchange
- Exchange: pushes messages to Queues
  - Exchange Type defines how messages are handled
- Consumer: application that receives messages

## Process summary
- All:
  - create Connection
  - create Channel
  - declare Exchange
- Emit:
  - Publish message to Exchange
- Receive: Either
  - Delare Queue
    - create Queue Binding to an Exchange
  - Use routing key and Exchange
  -> Consume from Queue

# rabbitmq container
```
docker run -d --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:3.11-management
docker exec -it rabbitmq /bin/bash 
  > rabbitmqctl list_connections
  > rabbitmqctl list_channels
  > rabbitmqctl list_queues
  > rabbitmqctl list_bindings
```

# curl SSE
```
curl -N --http2 -H "Accept:text/event-stream" http://localhost:8080/v0/events/6343d4e4-51bb-4f0b-9be2-3ee31cce9de3

curl --request POST \
  --url "http://localhost:8080/v0/dummy" \
  --header "Content-Type: application/json" \
  --data '{
    "request_ids": [
        "6343d4e4-51bb-4f0b-9be2-3ee31cce9de3"
      ]
    }'
  }
```
