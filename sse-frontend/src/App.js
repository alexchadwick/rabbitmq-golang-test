import logo from './logo.svg';
import './App.css';
import React, { useState, useEffect } from 'react';
import GetEvents from './GetEvents';

function App() {
  return (
    <div className="App">
      <GetEvents />
    </div>
  );
}

export default App;
