import React, { useState, useEffect } from 'react';

export default function GetEvents() {
  // const [ facts, setFacts ] = useState([]);
  const [events, setEvents] = useState(null);
  const [tracker, setTracker] = useState(null);
  const data = { request_ids: ["6343d4e4-51bb-4f0b-9be2-3ee31cce9de3"] };

  // Get initial data from dummy Consumer service Tracker endpoint
  useEffect( () => {
    fetch('http://localhost:8080/v0/dummy', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })
      .then( response => response.json() )
      .then( data => setTracker(data[0]) )
      .catch((error) => {
        console.error('Tracker Error:', error);
      })
  }, []); 

  // Open the SSE connection after Mount and close on Unmount
  useEffect( () => {
    if (!tracker) return;

    const evtSource = new EventSource(
      'http://localhost:8080/v0/' + tracker.request_id + "/events"
    )
    setEvents(evtSource)

    return () => evtSource.close();
  }, [tracker]);

  // Use SSE events
  useEffect( () => {
    if (!events) return;

    events.onopen = (e) => {
      console.log("The connection has been established. EventSource: ", e);
      console.log(events)

      console.log("Waiting for Events...");
    };
    
    events.onerror = (e) => {
      console.log("An error occurred while attempting to connect. Error: ", e);
    };

    events.onmessage = (e) => {
      console.log("Event: ", e);
    };
  }, [events]);

  return (
    <React.Fragment>
      <div className="GetEvents"></div>
    </React.Fragment>
  );
}
