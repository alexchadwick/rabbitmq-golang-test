// RabbitMQ resources
// Based on example of automatic reconnect here - https://raw.githubusercontent.com/rabbitmq/amqp091-go/feb4ba5dd2d04ea8bdb625460478eff9c52eaa03/example_client_test.go

package queue

import (
	"context"
	"errors"
	"time"

	amqp "github.com/rabbitmq/amqp091-go"
	"github.com/rs/zerolog"
)

type Client struct {
	Log             zerolog.Logger
	connection      *amqp.Connection
	Channel         *amqp.Channel
	exchange        Exchange
	queue           Queue
	deliverMode     uint8
	routingKey      string
	SubQoSPrefetch  int
	Done            chan bool
	notifyConnClose chan *amqp.Error
	notifyChanClose chan *amqp.Error
	notifyConfirm   chan amqp.Confirmation
	IsReady         bool
}

type Exchange struct {
	Name    string
	Kind    string // Direct or Topic
	Durable bool   // Will survive server restarts and remain declared when there are no remaining bindings
}

type Queue struct {
	Name       string
	Durable    bool // Value must match relevant Exchange.durable
	AutoDelete bool
	Exclusive  bool // Accessible by one connection
}

const (
	// When waiting for a initial connection to be established
	WaitConnectDelay = 500 * time.Millisecond

	// When reconnecting to the server after connection failure
	ReconnectDelay = 5 * time.Second

	// When setting up the channel after a channel exception
	reInitDelay = 2 * time.Second

	// When resending messages the server didn't confirm
	resendDelay = 5 * time.Second
)

var (
	errNotConnected  = errors.New("not connected to a server")
	errAlreadyClosed = errors.New("already closed: not connected to the server")
	errShutdown      = errors.New("client is shutting down")
)

// Creates a RabbitMQ client, and automatically attempts to connect to the
// server
func New(addr string, routingKey string, deliveryMode uint8, QoSPrefetch int, queue Queue, exchange Exchange, l zerolog.Logger) *Client {
	client := Client{
		Log:            l,
		queue:          queue,
		exchange:       exchange,
		deliverMode:    deliveryMode,
		routingKey:     routingKey,
		SubQoSPrefetch: QoSPrefetch,
		Done:           make(chan bool),
	}
	go client.handleReconnect(addr)
	return &client
}

// Continuously attempts to connect until it succeeds or the Done channel is
// closed. If the connection fails, it waits for ReconnectDelay before trying
// again. If the connection is successful, it calls the handleReInit function
// passing the established connection
func (client *Client) handleReconnect(addr string) {
	for {
		client.IsReady = false
		client.Log.Debug().
			Str("routing_key", client.routingKey).
			Msg("Attempting to connect")

		conn, err := client.connect(addr)

		if err != nil {
			client.Log.Debug().
				Str("routing_key", client.routingKey).
				Msg("Failed to connect. Retrying...")

			select {
			case <-client.Done:
				return
			case <-time.After(ReconnectDelay):
			}
			continue
		}

		// If connection successful, init channel, exchange and queues
		if done := client.handleReInit(conn); done {
			break
		}
	}
}

// Create a new AMQP connection
func (client *Client) connect(addr string) (*amqp.Connection, error) {
	conn, err := amqp.Dial(addr)
	if err != nil {
		return nil, err
	}

	client.changeConnection(conn)
	client.Log.Debug().
		Str("routing_key", client.routingKey).
		Msg("Connected")
	return conn, nil
}

// handleReconnect will wait for a channel error
// and then continuously attempt to re-initialize both channels
func (client *Client) handleReInit(conn *amqp.Connection) bool {
	for {
		client.IsReady = false

		err := client.init(conn)

		if err != nil {
			client.Log.Printf("Failed to initialize channel. Retrying: %w", err)

			select {
			case <-client.Done:
				return true
			case <-time.After(reInitDelay):
			}
			continue
		}

		select {
		case <-client.Done:
			return true
		case <-client.notifyConnClose:
			client.Log.Print("Connection closed. Reconnecting...")
			return false
		case <-client.notifyChanClose:
			client.Log.Print("Channel closed. Re-running init...")
		}
	}
}

// init will initialize channel & declare queue
func (client *Client) init(conn *amqp.Connection) error {
	ch, err := conn.Channel()
	if err != nil {
		return err
	}

	err = ch.Confirm(false)
	if err != nil {
		return err
	}

	err = ch.ExchangeDeclare(
		client.exchange.Name,
		client.exchange.Kind,
		client.exchange.Durable,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return err
	}

	client.AddQueue(ch, client.routingKey)

	client.IsReady = true
	client.changeChannel(ch)
	client.Log.Debug().
		Str("routing_key", client.routingKey).
		Msg("Setup")

	return nil
}

// Add queue to existing channel
func (client *Client) AddQueue(ch *amqp.Channel, routingKey string) error {
	// Don't declare queue if it's empty and no routing key is provided
	if client.queue != (Queue{}) && routingKey != "" {
		client.routingKey = routingKey

		_, err := ch.QueueDeclare(
			client.queue.Name,
			client.queue.Durable,
			client.queue.AutoDelete,
			client.queue.Exclusive,
			false, // No-wait
			nil,   // Arguments
		)
		if err != nil {
			return err
		}

		err = ch.QueueBind(
			client.queue.Name,
			client.routingKey,
			client.exchange.Name,
			false,
			nil,
		)
		if err != nil {
			return err
		}
		client.Log.Debug().
			Str("routing_key", client.routingKey).
			Msg("Setup queue")
	}
	return nil
}

// changeConnection takes a new connection to the queue,
// and updates the close listener to reflect this.
func (client *Client) changeConnection(connection *amqp.Connection) {
	client.connection = connection
	client.notifyConnClose = make(chan *amqp.Error, 1)
	client.connection.NotifyClose(client.notifyConnClose)
}

// changeChannel takes a new channel to the queue,
// and updates the channel listeners to reflect this.
func (client *Client) changeChannel(channel *amqp.Channel) {
	client.Channel = channel
	client.notifyChanClose = make(chan *amqp.Error, 1)
	client.notifyConfirm = make(chan amqp.Confirmation, 1)
	client.Channel.NotifyClose(client.notifyChanClose)
	client.Channel.NotifyPublish(client.notifyConfirm)
}

// Push will push data onto the queue, and wait for a confirm.
// If no confirms are received until within the resendTimeout,
// it continuously re-sends messages until a confirm is received.
// This will block until the server sends a confirm. Errors are
// only returned if the push action itself fails, see UnsafePush.
func (client *Client) Push(data []byte, routingKey string) error {
	if !client.IsReady {
		return errors.New("failed to push: not connected")
	}
	for {
		err := client.UnsafePush(data, routingKey)
		if err != nil {
			client.Log.Print("Push failed. Retrying...")
			select {
			case <-client.Done:
				return errShutdown
			case <-time.After(resendDelay):
			}
			continue
		}
		select {
		case confirm := <-client.notifyConfirm:
			if confirm.Ack {
				client.Log.Print("Push confirmed")
				return nil
			}
		case <-time.After(resendDelay):
		}
		client.Log.Print("Push didn't confirm. Retrying...")
	}
}

// UnsafePush will push to the queue without checking for
// confirmation. It returns an error if it fails to connect.
// No guarantees are provided for whether the server will
// receive the message.
func (client *Client) UnsafePush(data []byte, routingKey string) error {
	if !client.IsReady {
		return errNotConnected
	}

	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	return client.Channel.PublishWithContext(
		ctx,
		client.exchange.Name,
		routingKey,
		false, // Mandatory
		false, // Immediate
		amqp.Publishing{
			DeliveryMode: client.deliverMode,
			Timestamp:    time.Now(),
			ContentType:  "text/plain",
			Body:         data,
		},
	)
}

// Consume will continuously put queue items on the channel. It is required to
// call delivery.Ack when it has been successfully processed, or delivery.Nack
// when it fails. Ignoring this will cause data to build up on the server.
func (client *Client) Consume() (<-chan amqp.Delivery, error) {
	if !client.IsReady {
		return nil, errNotConnected
	}

	if err := client.Channel.Qos(
		client.SubQoSPrefetch, // prefetchCount
		0,                     // prefrechSize
		false,                 // global
	); err != nil {
		return nil, err
	}

	return client.Channel.Consume(
		client.queue.Name,
		"",                     // Consumer
		false,                  // Auto-Ack
		client.queue.Exclusive, // Exclusive
		false,                  // No-local
		false,                  // No-Wait
		nil,                    // Args
	)
}

// Close will cleanly shutdown the channel and connection.
func (client *Client) Close() error {
	if !client.IsReady {
		return errAlreadyClosed
	}
	close(client.Done)
	err := client.Channel.Close()
	if err != nil {
		return err
	}
	err = client.connection.Close()
	if err != nil {
		return err
	}

	client.IsReady = false
	return nil
}

// Wait for the client to be ready, or to be closed.
func Wait(client *Client, delay time.Duration, ctx context.Context) {
	for {
		if !client.IsReady {
			client.Log.Debug().
				Str("routing_key", client.routingKey).
				Msg("Waiting for client to be ready...")

			select {
			case <-ctx.Done():
				return
			case <-time.After(delay):
			}
			continue
		} else {
			client.Log.Debug().
				Str("routing_key", client.routingKey).
				Msg("Client is ready")
			break
		}
	}
}
