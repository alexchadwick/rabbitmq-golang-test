package main

import (
	"context"
	"log"
	"net/http"
	"os/signal"
	"syscall"
	"time"

	amqp "github.com/rabbitmq/amqp091-go"
)

type application struct {
	ctx     context.Context
	name    string
	channel *amqp.Channel
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Panicf("%s: %s", msg, err)
	}
}

func main() {
	// Graceful shutdown, context listens for interrupt signal from OS
	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()

	// RabbitMQ Conection and Channel, reusable
	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	// Ensure RabbitMQ Exchange exits
	// Not in handler to reduce number of operations/request
	err = ch.ExchangeDeclare(
		"laterail.event", // name
		"topic",          //"fanout",  // type
		false,            // durable
		false,            // auto-deleted
		false,            // internal
		false,            // no-wait
		nil,              // arguments
	)
	failOnError(err, "Failed to declare an exchange")

	// Instantiate app
	app := &application{
		ctx:     ctx,
		name:    "receive",
		channel: ch,
	}

	// Initialise server in a goroutine, won't block graceful shutdown handling below
	server := &http.Server{
		Addr:    "0.0.0.0" + ":" + "8080",
		Handler: app.routes(),
		// IdleTimeout:  time.Minute,
		// ReadTimeout:  5 * time.Second,
		// WriteTimeout: 5 * time.Second,
	}
	go func() {
		err := server.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			log.Print("chi server failed")
		}
	}()
	log.Printf("listening on %s", server.Addr)

	// Listen for interrupt signal and notify user of shutdown.
	<-ctx.Done()
	stop()
	log.Print("shutting down gracefully, press Ctrl+C again to force")

	// Shutdown server and db connections, timeout 5 seconds
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := server.Shutdown(ctx); err != nil {
		log.Print("Server forced to shutdown")
	}
}
