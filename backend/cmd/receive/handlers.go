package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/go-chi/chi/v5"
)

var pingInterval = time.Second * 10

// Get progress and results of existing request IDs
func (app application) tracker(w http.ResponseWriter, r *http.Request) {
	h := w.Header()
	h.Set("Content-Type", "text/event-stream")
	h.Set("Cache-Control", "no-cache")
	h.Set("Connection", "keep-alive")
	h.Set("X-Accel-Buffering", "no")

	// Get request id from URL
	request_id := chi.URLParam(r, "id")

	flusher, ok := w.(http.Flusher)
	if !ok {
		return
	}

	io.WriteString(w, "Connected to SSE Service\n\n")
	flusher.Flush()

	q, err := app.channel.QueueDeclare(
		"",    // name
		false, // durable
		true,  // delete when consumer disconnects
		true,  // exclusive
		false, // no-wait
		nil,   // arguments
	)
	failOnError(err, "Failed to declare a queue")

	// Only return events for request_id to client
	err = app.channel.QueueBind(
		q.Name,           // queue name
		request_id,       // routing key
		"laterail.event", // exchange
		false,
		nil,
	)
	failOnError(err, "Failed to bind a queue")

	msgs, err := app.channel.Consume(
		q.Name, // queue
		"",     // consumer
		false,  // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	failOnError(err, "Failed to register a consumer")

	// Actions for go channel operations
	for {
		select {
		case <-r.Context().Done():
			log.Print("client closed the connection")

			// Delete Queue when client disconnects
			_, err = app.channel.QueueDelete(
				q.Name, // name
				false,  // ifUnused
				false,  // ifEmpty
				true,   // no-wait
			)
			failOnError(err, "Failed to delete a queue on client disconnect")
			return
		case <-app.ctx.Done():
			log.Print("service closed the connection. Stopping...")
			io.WriteString(w, "data: service closed the connection. Stopping...\n\n")
			flusher.Flush()
			return
		case <-time.After(pingInterval):
			io.WriteString(w, "data:  heart beat\n\n")
			flusher.Flush()
		case message := <-msgs:
			log.Printf("%s: %s", request_id, message.Body)

			// Write response to client
			fmt.Fprintf(w, "data: %s: %s\n\n", request_id, message.Body)
			flusher.Flush()

			// Send positive acknowledgment
			if err := message.Ack(false); err != nil {
				fmt.Printf("Error acknowledging message: %s\n", err)
			}
		}
	}
}

type tracker struct {
	IDs []string `json:"request_ids"`
}

// Mock response from Consumer Tracker endpoint
func (app application) dummy(w http.ResponseWriter, r *http.Request) {
	// Decode json POST body
	decoder := json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()
	var request tracker
	if err := decoder.Decode(&request); err != nil {
		errMsg := "failed to decode POST body"
		http.Error(w, errMsg, http.StatusBadRequest)
		return
	}
	log.Printf("POST Body: %+v", request)

	responseBytes, err := os.ReadFile("./cmd/receive/consumer_response.json")
	if err != nil {
		log.Printf("Error getting dummy consumer response from file. Error: %s", err)
	}

	var responseArray interface{}
	json.Unmarshal(responseBytes, &responseArray)

	// Respond to client
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(responseArray)
	if err != nil {
		errMsg := "{\"message\": \"failed to encode completed tracker response\"}"
		log.Print(errMsg)
		http.Error(w, errMsg, http.StatusInternalServerError)
		return
	}
}
