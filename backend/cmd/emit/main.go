package main

import (
	"context"
	"os"
	"os/signal"
	"sse-services/internal/queue"
	"syscall"

	"github.com/rs/zerolog"
)

const (
	first  = "6343d4e4-51bb-4f0b-9be2-3ee31cce9de3"
	second = "second"
)

func main() {
	// Graceful shutdown, context listens for interrupt signal from OS
	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()
	log := zerolog.New(os.Stdout)

	// Start queue actions
	emitHSP, receiveHSP, emitEvents := initMessages(log)
	go Emit(emitHSP, []byte("hsp"), "laterail.submit", ctx)
	go Receive(receiveHSP, ctx)

	// test routing key used in dummy frontend
	queue.Wait(emitEvents, queue.WaitConnectDelay, ctx)
	go Emit(emitEvents, []byte(first), first, ctx)

	// second routing key
	go Emit(emitEvents, []byte(second), second, ctx)

	// Graceful stop
	<-ctx.Done()
	// Close only one of emitters. Both share the same channel
	emitEvents.Close()
	log.Info().Msg("Stopped")
}
