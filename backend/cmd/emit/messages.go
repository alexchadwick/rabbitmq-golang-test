package main

import (
	"context"
	"sse-services/internal/queue"
	"time"

	amqp "github.com/rabbitmq/amqp091-go"
	"github.com/rs/zerolog"
)

func initMessages(logger zerolog.Logger) (emitHSP, receiveHSP, emitEvents *queue.Client) {
	// This exports a Client object that wraps this library. It
	// automatically reconnects when the connection fails, and
	// blocks all pushes until the connection succeeds. It also
	// confirms every outgoing message, so none are lost.
	// It doesn't automatically ack each message, but leaves that
	// to the parent process, since it is usage-dependent.
	emitHSP = queue.New(
		"amqp://guest:guest@localhost:5672/",
		"laterail.submit",
		amqp.Persistent,
		1,
		queue.Queue{
			Name:       "laterail.submit",
			Durable:    true,
			AutoDelete: false,
			Exclusive:  false,
		},
		queue.Exchange{
			Name:    "laterail.submit",
			Kind:    "direct",
			Durable: true,
		},
		logger)

	receiveHSP = queue.New(
		"amqp://guest:guest@localhost:5672/",
		"laterail.submit",
		amqp.Persistent,
		1,
		queue.Queue{
			Name:       "laterail.submit",
			Durable:    true,
			AutoDelete: false,
			Exclusive:  false,
		},
		queue.Exchange{
			Name:    "laterail.submit",
			Kind:    "direct",
			Durable: true,
		},
		logger)

	// Will use temporary queues in chi handler
	emitEvents = queue.New(
		"amqp://guest:guest@localhost:5672/",
		"",
		amqp.Transient,
		1,
		queue.Queue{},
		queue.Exchange{
			Name:    "laterail.event",
			Kind:    "topic",
			Durable: false,
		},
		logger)

	return emitHSP, receiveHSP, emitEvents
}

// Pushes a message to the message exchange every 100ms. Does not close the
// connection and channel. Enables multiple emitters, sharing a connection and
// channel.
func Emit(client *queue.Client, message []byte, routingKey string, ctx context.Context) {
	queue.Wait(client, queue.WaitConnectDelay, ctx)

	for {
		select {
		case <-time.After(100 * time.Millisecond):
			if err := client.Push(message, routingKey); err != nil {
				client.Log.Printf("Push failed: %s", err)
			} else {
				client.Log.Print("Push succeeded!")
			}
		case <-ctx.Done():
			return
		}
	}
}

// Waits for the connection to be ready, and then starts consuming messages from
// the message queue. Listens to channels to stop the consumer and detect
// channel closure events respectively. On each iteration of the loop, if
// ctx.Done() is closed, the consumer closes the connection and returns. If
// chClosedCh receives a channel closure event, the consumer tries to
// re-establish the connection and resume consuming. If a new message is
// received, the function acknowledges it and prints the message body. Closes
// the connection or channel, so must be the only consumer.
func Receive(client *queue.Client, ctx context.Context) {
	queue.Wait(client, queue.WaitConnectDelay, ctx)

	deliveries, err := client.Consume()
	if err != nil {
		client.Log.Printf("Could not start consuming: %s", err)
		return
	}

	chClosedCh := make(chan *amqp.Error, 1)
	client.Channel.NotifyClose(chClosedCh)

	for {
		select {
		case <-ctx.Done():
			return

		case amqErr := <-chClosedCh:
			// This case handles the event of closed channel e.g. abnormal shutdown
			client.Log.Printf("AMQP Channel closed due to: %s", amqErr)

			deliveries, err = client.Consume()
			if err != nil {
				// If the AMQP channel is not ready, it will continue the loop. Next
				// iteration will enter this case because chClosedCh is closed by the
				// library
				client.Log.Print("Error trying to consume, will try again")
				continue
			}

			// Re-set channel to receive notifications
			// The library closes this channel after abnormal shutdown
			chClosedCh = make(chan *amqp.Error, 1)
			client.Channel.NotifyClose(chClosedCh)

		case delivery := <-deliveries:
			// Handle new message
			client.Log.Printf("Received message: %s", delivery.Body)

			// Send positive acknowledgment
			if err := delivery.Ack(false); err != nil {
				client.Log.Printf("Error acknowledging message: %s", err)
			}
		}
	}
}
